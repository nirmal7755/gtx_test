import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;


public class Filter {
	
	public WebDriver filterByCommonSNP(WebDriver driver, String allele_freq, int hgmd) {
		
		driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[3]/img[2]")).click();
		
		if(driver.findElement(By.id("remove_more")).isDisplayed()) {
			driver.findElement(By.id("remove_more")).click();
		}
		
		if(driver.findElement(By.id("fi_csnp")).isDisplayed()) {
			driver.findElement(By.id("fi_csnp")).click();
		}
		
		Select dropdown = new Select(driver.findElement(By.id("bg_hgmd_percentage")));
		dropdown.selectByVisibleText(allele_freq);
		
		if(hgmd == 1) {
			if(!driver.findElement(By.id("fi_chgmd")).isSelected()) {
				driver.findElement(By.id("fi_chgmd")).click();
			}
		} else if(hgmd == 0) {
			if(driver.findElement(By.id("fi_chgmd")).isSelected()) {
				driver.findElement(By.id("fi_chgmd")).click();
			}
		}
		
		driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[3]/img[2]")).click();
		
		return driver;
	}
	
	public WebDriver filterByBackgroundVariants(WebDriver driver,String path) throws IOException {
		
		driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[3]/img[2]")).click();
				
        String cmd = "C:\\Users\\Nirmal\\Desktop\\upload.exe " + path;
		Runtime.getRuntime().exec(cmd);
		driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[3]/div/table/tbody/tr[1]/td[4]/span[2]/table/tbody/tr/td/div[1]/input[2]")).click();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[3]/img[2]")).click();
		
		return driver;
	}

}
