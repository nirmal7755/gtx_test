import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class InputOptions {
	
	
	public WebDriver uploadSampleFile(WebDriver driver) {
		driver.findElement(By.id("samplefile")).click();
		return driver;
	}
		
	public WebDriver uploadFromInputBox(WebDriver driver, String data){
		driver.findElement(By.id("uc")).sendKeys(data);
		return driver;
	}
	
	public WebDriver uploadFile(WebDriver driver, String path, int timeoutInSeconds) throws IOException {
		
		

		String cmd = "C:\\Users\\Nirmal\\Desktop\\upload.exe " + path;
		
		Runtime.getRuntime().exec(cmd);

		
		if(driver.findElement(By.id("uploadConfirm")).isDisplayed()) {
			driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[2]/table/tbody/tr[1]/td[2]/span[2]/div[1]/div[1]/div[1]/div[1]/input[3]")).click();
		}
		else {
			driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[2]/table/tbody/tr[1]/td[2]/span[2]/div[1]/div[1]/div[1]/div[1]/input[2]")).click();
		}
		
		WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
		WebElement uploadconfirm = driver.findElement(By.id("uploadConfirm"));
		wait.until(ExpectedConditions.textToBePresentInElement(uploadconfirm,"Uploaded file recognized"));
		
		return driver;
	}

}
