import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class Tracks {
	
	public WebDriver selectTracks (WebDriver driver, String trackname) {
		
		String[] track_names = trackname.split(",");
		List<String> tracks = Arrays.asList(track_names);
		
		for(int i=0; i<tracks.size(); i++){
			if(!driver.findElement(By.id(tracks.get(i))).isSelected()) {
				driver.findElement(By.id(tracks.get(i))).click();
			}
		}
		
		return driver;
		

	}
	
    public WebDriver selectAllTracks (WebDriver driver, String trackname) {
		
		String[] track_names = trackname.split(",");
		List<String> tracks = Arrays.asList(track_names);
		
		for(int i=0; i<tracks.size(); i++){
			String track_id = tracks.get(i);
			if( track_id.endsWith("_coords") ) {
				continue;
			}
			if(!driver.findElement(By.id(track_id)).isSelected()) {
				driver.findElement(By.id(tracks.get(i))).click();
			}
		}
		
		return driver;
		

	}
	
public WebDriver unselectAllTracks (WebDriver driver, String trackname) {
		
	    String[] track_names = trackname.split(",");
		List<String> tracks = Arrays.asList(track_names);
		
		for(int i=0; i<tracks.size(); i++){
			if(driver.findElement(By.id(tracks.get(i))).isSelected()) {
				driver.findElement(By.id(tracks.get(i))).click();
			}
		}
		
		return driver;
		

	}
	
	
	

}
