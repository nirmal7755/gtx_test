import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//test

public class Annotate {
	
	public WebDriver annotateResults (WebDriver driver, int timeInMinutes){
		driver.findElement(By.id("annotateButton")).click();
		
		int timeInSeconds = timeInMinutes * 60;
		
		System.out.println(timeInSeconds);
		
		WebDriverWait wait = new WebDriverWait(driver, timeInSeconds);
		wait.until(ExpectedConditions.titleContains("Genome Trax - Search Results"));
		
		return driver;
		
	}
	
	public WebDriver testTabGeneration(WebDriver driver, HashMap<String,String> tabstatus){
		String data = checkTab(driver,"ui-id-1","dataGene");
		String status = tabstatus.get("gene").toString();
		System.out.println(status);
		return driver;
		//if(status.contentEquals(0)) {
		//  if( data.isEmpty() || data.contains("No data available in table"))
	    // }
	   //}
	}
	
	public String checkTab(WebDriver driver,String tab_id,String tab_data_id) {
		WebElement gene_tab = driver.findElement(By.id(tab_id));
		gene_tab.click();
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id(tab_data_id))));
		WebElement datatable = driver.findElement(By.id(tab_data_id));
		String data = datatable.getText();
		
		
		
		return data;
		
	}
	
	
	//public Webdriver 
	
    
//- See more at: http://www.java2novice.com/java-collections-and-util/hashmap/basic-operations/#sthash.L76ZJppJ.dpuf
	
	
	/*
	 * my $gene_tab = $self->driver->find_element('ui-id-1', 'id');
    $gene_tab->click();
	$self->tab("gene");
	$self->driver->pause(5000);
    my $datatable = $self->driver->find_element('dataGene', 'id');
    my $gene_data = $datatable->get_text();
	my $exp_tab_status = $self->expval_gene;
	
	$tabs_result = 
      testTabGeneration($self, $exp_tab_status, $gene_data, $tabs_result);
      
       sub testTabGeneration {
    my ($self, $exp_tab_status, $data, $res_tab_status ) = @_;
		
	if($exp_tab_status == 1) {
	  if(length($data)) {
		$res_tab_status->{$self->tab} = 
		             "1:".$self->tab ." tab is generated properly as expected."; 
	  }
	  else {
		$res_tab_status->{$self->tab} = 
		     "0:".$self->tab ." tab is not generated properly as expected.";
	  }
	} elsif($exp_tab_status == 0) {
	  if(length($data) && $data ne "No data available in table") { #tried defined but not working
		$res_tab_status->{$self->tab} = 
		    "0: ".$self->tab ." tab is not generated properly as expected.";
	  }
	  else {
		$res_tab_status->{$self->tab} = 
		   "1: ".$self->tab ." tab is generated properly as expected.";
	  }
	} else {
	  print "Expected value for ". $self->tab. " tab is not defined in test script\n"; 
	}
	
	return $res_tab_status;
}

	 */

}
