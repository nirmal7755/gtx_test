


public class Config {
 public String URL = "https://www.genometrax.biobase-international.com";
 public String browser = "FireFox";
 public String username ="generaluser";
 public String password ="generaluser@bio";
 public String[] tracks = {"hgmd","hgmdeq","pgmd","clinvar","gwas","cosmic","evs","dbnsfp","fulldbsnp","transfac_site","chip","dnase","cpg","microsatellite","virtualtss","ptm","mirna","disease","drug","pathway","hgmd_disease_genes","orpha","omim","snpeff"};
}
