//import java.io.IOException;

import java.io.IOException;
import java.util.Properties;









import org.junit.Assert;
import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class FileRecognition extends GenomeTrax{
    
	@BeforeMethod
	public void testHomePage() throws IOException {
		goToHomePage();
		driver.manage().window().maximize();
	}
	
	@AfterMethod
	public void closeBrowser() throws IOException {
		driver.quit();
	}
	
	@Test
	public void BED() throws IOException, InterruptedException {
		test("BED","bed");	
	}

    @Test
    public void twentyThreeAndMe() throws IOException, InterruptedException {
	  test("23AndMe", "txt");
	}
    
    @Test
    public void CG() throws IOException, InterruptedException {
	  test("CG", "tsv");
	}
    
    @Test
    public void EGID() throws IOException, InterruptedException {
	  test("EGID", "txt");
	}
    
    @Test
    public void ENSEMBL() throws IOException, InterruptedException {
	  test("ENSEMBL", "txt");
	}
    
    @Test
    public void GFF() throws IOException, InterruptedException {
	  test("GFF", "txt");
	}
    
    @Test
    public void HGNC() throws IOException, InterruptedException {
	  test("HGNC", "txt");
	}
    
    @Test
    public void MIRNA() throws IOException, InterruptedException {
	  test("MIRNA", "txt");
	}
    
    @Test
    public void SNP() throws IOException, InterruptedException {
	  test("SNP", "txt");
	}
    
    @Test
    public void UCSC() throws IOException, InterruptedException {
	  test("UCSC", "txt");
	}
    
    @Test
    public void UNIPROT() throws IOException, InterruptedException {
	  test("UNIPROT", "txt");
	}
    
    @Test
    public void VCF() throws IOException, InterruptedException {
	  test("VCF", "vcf");
	}
    
    
    
    
    
    
    
    

   
    public void test(String type, String extn) throws IOException, InterruptedException {
	
	Properties conf = getGlobalParams();
	String filepath = 
			conf.getProperty("testinput_path") + type + "\\" + type+"_test."+extn;
			System.out.println(filepath);
			uploadFile(filepath, 20);
			
			
			String ActualValue = driver.findElement(By.id("uploadConfirm")).getText();
			String ExpectedValue = "Uploaded file recognized as " + type; 
	        Assert.assertEquals(ExpectedValue, ActualValue);
		}
}
	
		



//input options
//obj.uploadSampleFile(obj.driver);
//obj.uploadFromInputBox(obj.driver, "rs00001");
//obj.uploadFile(obj.driver, "C:\\Users\\Nirmal\\Desktop\\rsid.txt");
//obj.filterByCommonSNP(obj.driver, "2%", 1);
//obj.filterByBackgroundVariants(obj.driver, "C:\\Users\\Nirmal\\Desktop\\test.bed");
//obj.limitToExonicVariants(obj.driver);
//obj.limitToGenesForDiseases(obj.driver, "Alzheimer Disease");
//obj.limitToGeneList(obj.driver, "C:\\Users\\Nirmal\\Desktop\\test.bed");