import java.io.IOException;
import java.util.HashMap;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class testpage extends GenomeTrax {
	
	@BeforeMethod
	public void HomePage() throws IOException {
		goToHomePage();
		driver.manage().window().maximize();
	}
	
	//@AfterMethod
	//public void closeBrowser() throws IOException {
	//	driver.quit();
	//}
	
	@Test
	public void hello () throws IOException{
		uploadSampleFile();
		selectTracks("hgmd,hgmdeq");
		
		HashMap h = expectedTabStatus("gene=1,variant=1,filtered=0,annotated=1,unmatched=1");
		annotateResults(1);
		testTabGeneration(h);
	}
	

}
