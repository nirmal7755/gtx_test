import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;


public class Browser {
	
	public WebDriver driver;
	
	public WebDriver launchBrowser(String browser) {
	
		if(browser.equals("FireFox")){
			driver = new FirefoxDriver();
		} else if(browser.equals("Chrome")){
			driver = new ChromeDriver();
		} else if(browser.equals("InternetExplorer")) {
			driver = new InternetExplorerDriver();
		}
		
		return driver;
		
	}
}
