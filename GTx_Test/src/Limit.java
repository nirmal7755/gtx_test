

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class Limit {
	
	public WebDriver limitToExonicVariants(WebDriver driver) {
		
		driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[4]/img[2]")).click();
		
		if(!driver.findElement(By.id("exon_limit")).isSelected()) {
			driver.findElement(By.id("exon_limit")).click();
		}
		
		driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[4]/img[2]")).click();
		
		return driver;
	}
	
     public WebDriver limitToGenesForDiseases(WebDriver driver, String disease) {
    	 
    	 driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[4]/img[2]")).click();
    	 
    	 if(!driver.findElement(By.id("disease_limit")).isSelected()) {
 			driver.findElement(By.id("disease_limit")).click();
 		}
    	 
    	driver.findElement(By.id("dl")).sendKeys(disease);
    	
    	try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
     	driver.findElement(By.xpath("/html/body/ul/li/a")).click();
        
    	driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[4]/img[2]")).click();
    	 
		return driver;
	}
     
    public WebDriver limitToGeneList(WebDriver driver, String path) throws IOException {
    	
    	driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[4]/img[2]")).click();	
    	
    	String cmd = "C:\\Users\\Nirmal\\Desktop\\upload.exe " + path;
 		Runtime.getRuntime().exec(cmd);
 		driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/form/div[4]/div/table/tbody/tr[1]/td[5]/span[2]/table/tbody/tr/td/div[1]/input[2]")).click();
 		
 		try {
 			Thread.sleep(5000);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
 		
 		return driver;
    }
	
	

}
