import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class GenomeTrax {
    
	public WebDriver driver=null;
	public Config cfg;
    
	public void goToHomePage() throws IOException {
		Browser b = new Browser();
		Properties cfg = getGlobalParams();
		LoginPage p = new LoginPage();
		
		
		driver = b.launchBrowser(cfg.getProperty("browser"));
		driver = p.goToLoginPage(driver, cfg.getProperty("URL"));
		
		driver.findElement(By.id("username")).sendKeys(cfg.getProperty("username"));
		driver.findElement(By.id("password")).sendKeys(cfg.getProperty("password"));
		driver.findElement(By.id("login_submit")).click();

	}
	
	
	public void uploadSampleFile() {
		InputOptions inp = new InputOptions();
		driver = inp.uploadSampleFile(driver);
	}
	
	public void uploadFromInputBox(WebDriver driver_local, String data) {
		InputOptions inp = new InputOptions();
		driver = inp.uploadFromInputBox(driver_local, data);
	}
	
	public void uploadFile(String data, int time) throws IOException, InterruptedException {
		InputOptions inp = new InputOptions();
		driver = inp.uploadFile(driver, data, time);
	}
	
	public void filterByCommonSNP(String allele_freq, int hgmd) {
		Filter fr = new Filter();
		driver = fr.filterByCommonSNP(driver, allele_freq, hgmd);
	}
	
	public void filterByBackgroundVariants(WebDriver driver_local,String path) throws IOException {
		Filter fr = new Filter();
		driver = fr.filterByBackgroundVariants(driver_local, path);
	}
	
	public void limitToGenesForDiseases(WebDriver driver_local, String disease) {
		Limit lt =new Limit();
		driver = lt.limitToGenesForDiseases(driver_local, disease);
	}
	
	public void limitToExonicVariants(WebDriver driver_local) {
		Limit lt = new Limit();
		driver = lt.limitToExonicVariants(driver_local);
	}
	
	public void limitToGeneList(WebDriver driver_local, String path) throws IOException {
		Limit lt = new Limit();
		driver = lt.limitToGeneList(driver_local, path);
	}
	
	public Properties getGlobalParams() throws IOException {
		Properties prop=new Properties();
		FileInputStream fis = new FileInputStream("D:\\eclipse\\Workspace\\GTx_Test\\src\\global.properties");
		prop.load(fis);
		return prop;
	}
	
	public void selectTracks(String trackname) throws IOException {
		Tracks tr = new Tracks();
		Properties cfg = getGlobalParams();
		driver = tr.unselectAllTracks(driver, cfg.getProperty("tracks"));
		driver = tr.selectTracks(driver, trackname);
	}
	
	public void selectAllTracks() throws IOException {
		Tracks tr = new Tracks();
		Properties cfg = getGlobalParams();
		driver = tr.selectAllTracks(driver, cfg.getProperty("tracks"));
	}
	
	public void unselectAllTracks(String trackname) {
		Tracks tr = new Tracks();
		driver = tr.unselectAllTracks(driver, trackname);
	}
	
	public void annotateResults (int timeInMinutes) {
		Annotate btn = new Annotate();
		driver = btn.annotateResults(driver, timeInMinutes);
		
	}
	
	public void testTabGeneration (HashMap<String,String> tabstatus) {
		Annotate btn = new Annotate();
		driver = btn.testTabGeneration(driver, tabstatus);
		
	}
	
	public HashMap<String, String> expectedTabStatus(String tabstatus) {
		
		String[] tab_status = tabstatus.split(",");
		List<String> tabList = Arrays.asList(tab_status);
		HashMap<String, String> hm = new HashMap<String, String>();
		
		for (String temp : tabList) {
			
			String[] test = temp.split("=");   
		    String name = test[0];   
		    String status = test[1];
		    
			hm.put(name, status);
			
	}
    return hm;
	
	}
	
	
	
	
}
